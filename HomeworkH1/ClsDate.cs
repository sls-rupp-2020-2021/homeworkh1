﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkH1
{
    public class ClsDate
    {
        private string m_StartDate;
        private string m_EndDate;
        public string StartDate
        {
            get { return m_StartDate; }
            set { m_StartDate = value; }
        }
        public string EndDate {
            get { return m_EndDate; }
            set { m_EndDate = value; }
        }
    }
}
