﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkH1
{
    public class ZClass
    {
        private ClsDegree m_Obj = new ClsDegree();
        public ClsDegree Degree
        {
            get { return m_Obj; }
        }
        public void PrintDegree()
        {
            System.Windows.Forms.MessageBox.Show(
                "Bachelor Degree" + "\n\t"
                    + "Start Date" + "\t\t" + "End Date" + "\n\t"
                    + m_Obj.Bachelor(0).StartDate + "\t\t" + m_Obj.Bachelor(0).EndDate + "\n\t"
                    + m_Obj.Bachelor(1).StartDate + "\t\t" + m_Obj.Bachelor(1).EndDate + "\n"
                    + "Master Degree" + "\n\t" +
                    "Start Date" + "\t\t" + "End Date" + "\n\t"
                    + m_Obj.Master(0).StartDate + "\t\t" + m_Obj.Master(0).EndDate + "\n\t"
                    + m_Obj.Master(1).StartDate + "\t\t" + m_Obj.Master(1).EndDate, "Print Degree"
                    );
        }
    }
}
