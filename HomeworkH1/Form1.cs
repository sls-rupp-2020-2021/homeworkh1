﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HomeworkH1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ZClass obj = new ZClass();
            obj.Degree.Bachelor(0).StartDate = "BSDate1";
            obj.Degree.Bachelor(0).EndDate = "BEDate1";
            obj.Degree.Bachelor(1).StartDate = "BSDate2";
            obj.Degree.Bachelor(1).EndDate = "BEDate2";
            //.........................................
            obj.Degree.Master(0).StartDate = "MSDate1";
            obj.Degree.Master(0).EndDate = "MEDate1";
            obj.Degree.Master(1).StartDate = "MSDate2";
            obj.Degree.Master(1).EndDate = "MEDate2";
            //.........................................
            //.........................................
            obj.PrintDegree(); //Display in MessageBox as following
        }
    }
}
