﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkH1
{
    public class ClsDegree
    {
        private ClsDate[] b_Obj;
        private ClsDate[] m_Obj;
        private int b;
        private int m;
        public ClsDate Bachelor(int i)
        {
            ClsDate tempBachelor = null;
            if(i == b)
            {
                Array.Resize(ref b_Obj, b + 1);
                b_Obj[b] = new ClsDate();
                tempBachelor = b_Obj[b];
                b +=1;
            }
            else if (i>=0 &&  i<b)
            {
                return b_Obj[i];
            }
            return tempBachelor;
        }
        public ClsDate Master (int i)
        {
            ClsDate tempMaster = null;  
            if(i==m)
            {
                Array.Resize(ref m_Obj, m + 1);   
                m_Obj[m] = new ClsDate();
                tempMaster = m_Obj[m];
                m+=1;
            }
            else if(i>=0 && i<m)
            {
                return m_Obj[i];
            }
            return tempMaster;
        }
    }
}
